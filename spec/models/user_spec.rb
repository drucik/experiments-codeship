require 'rails_helper'

RSpec.describe User, :type => :model do

  it 'should be valid' do
    user = User.new username: 'uzytkownik'

    expect(user).to be_valid
  end

  describe 'should not be valid' do

    it 'when username is empty' do
      user = User.new

      expect(user).to_not be_valid
    end

    it 'when username is too long' do
      user = User.new username: 'a'*51

      expect(user).to_not be_valid
    end
  end
end
